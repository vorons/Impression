use dbus::arg::{OwnedFd, RefArg, Variant};
use dbus::blocking::{Connection, Proxy};
use dbus_udisks2::{DiskDevice, Disks, UDisks2};
use futures::StreamExt;
use itertools::Itertools;
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::os::unix::io::FromRawFd;
use std::str;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;
use std::time::Duration;

use crate::task::Task;
use crate::window::DiskImage;

type UDisksOptions = HashMap<&'static str, Variant<Box<dyn RefArg>>>;

pub fn refresh_devices() -> Result<Vec<DiskDevice>, ()> {
    let udisks = UDisks2::new().map_err(|_| ())?;
    let devices = Disks::new(&udisks).devices;
    let devices = devices
        .into_iter()
        .filter(|d| d.drive.connection_bus == "usb" || d.drive.connection_bus == "sdio")
        .filter(|d| d.parent.size != 0)
        .sorted_by_key(|d| d.drive.id.clone())
        .collect_vec();
    Ok(devices)
}

#[derive(Clone, Debug, PartialEq)]
pub enum FlashPhase {
    Download,
    Copy,
    Read,
    Validate,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FlashStatus {
    Active(FlashPhase, f64),
    Done(Option<String>),
}

pub struct FlashRequest {
    source: DiskImage,
    destination: DiskDevice,
    sender: glib::Sender<FlashStatus>,
    is_running: Arc<AtomicBool>,
}

impl FlashRequest {
    pub fn new(
        source: DiskImage,
        destination: DiskDevice,
        sender: glib::Sender<FlashStatus>,
        is_running: Arc<AtomicBool>,
    ) -> Self {
        Self {
            source,
            destination,
            sender,
            is_running,
        }
    }

    pub fn perform(self) {
        let source = self.source;
        let device = self.destination;

        if !self.is_running.load(std::sync::atomic::Ordering::SeqCst) {
            return;
        }

        // Unmount the devices beforehand.
        udisks_unmount(&device.parent.path).ok();
        for partition in &device.partitions {
            udisks_unmount(&partition.path).ok();
        }

        if !self.is_running.load(std::sync::atomic::Ordering::SeqCst) {
            return;
        }

        let Ok(file) = udisks_open(&device.parent.path) else {
            self.sender
                .send(FlashStatus::Done(Some("Failed to open".to_string())))
                .expect("Concurrency Issues");

            return;
        };

        if !self.is_running.load(std::sync::atomic::Ordering::SeqCst) {
            return;
        }

        let image = match source {
            DiskImage::Local {
                path,
                filename: _,
                size: _,
            } => {
                let Ok(image) = std::fs::File::open(path) else {
                    self.sender
                        .send(FlashStatus::Done(Some("Failed to open image".to_string())))
                        .expect("Concurrency Issues");

                    return;
                };

                image
            }
            DiskImage::Online { url, name } => {
                let result_path =
                    std::env::var("XDG_CACHE_HOME").unwrap() + "/tmp/" + &name + ".iso";

                let downloading_path = result_path.clone();

                #[derive(thiserror::Error, Debug)]
                #[error("Error while getting total size")]
                struct TotalSize {}

                let downloading_sender = self.sender.clone();

                match futures::executor::block_on(async move {
                    let handle = std::thread::spawn(move || {
                        let rt = tokio::runtime::Builder::new_multi_thread()
                            .enable_all()
                            .build()
                            .expect("Cannot Build a runtime");

                        let handle = rt.spawn(async move {
                            let mut file = File::create(downloading_path)?;

                            let res = reqwest::get(url).await?;

                            let total_size = res.content_length().ok_or(TotalSize {})?;
                            let mut downloaded: u64 = 0;
                            let mut stream = res.bytes_stream();

                            while let Some(Ok(chunk)) = stream.next().await {
                                file.write_all(&chunk)?;
                                downloaded =
                                    std::cmp::min(downloaded + (chunk.len() as u64), total_size);
                                downloading_sender
                                    .send(FlashStatus::Active(
                                        FlashPhase::Download,
                                        downloaded as f64 / total_size as f64,
                                    ))
                                    .expect("Concurrency Issues");
                            }

                            Ok::<(), anyhow::Error>(())
                        });

                        rt.block_on(handle).expect("Concurrency Issue")
                    });

                    handle.join().expect("Concurrency Issue")
                }) {
                    anyhow::Result::Err(_) => {
                        self.sender
                            .send(FlashStatus::Done(Some("Failed to open image".to_string())))
                            .expect("Concurrency Issues");

                        return;
                    }
                    anyhow::Result::Ok(()) => {
                        let Ok(image) = std::fs::File::open(result_path) else {
                            self.sender
                                .send(FlashStatus::Done(Some("Failed to open image".to_string())))
                                .expect("Concurrency Issues");

                            return;
                        };

                        image
                    }
                }
            }
        };

        FlashRequest::load_file(image, file, &self.sender, self.is_running.clone());

        let _ = udisks_eject(&device.drive.path);
    }

    fn load_file(
        image: File,
        target_file: File,
        sender: &glib::Sender<FlashStatus>,
        is_running: Arc<AtomicBool>,
    ) {
        let mut task = Task::new(image.into(), sender, is_running, false);
        task.subscribe(target_file.into());

        let mut bucket = [0u8; 64 * 1024];

        let Ok(_) = futures::executor::block_on(task.process(&mut bucket)) else {
            sender
                .send(FlashStatus::Done(Some("Failed to open image".to_string())))
                .expect("Concurrency Issues");

            return;
        };
    }
}

fn udisks_eject(dbus_path: &str) -> Result<(), ()> {
    let connection = Connection::new_system().map_err(|_| ())?;

    let dbus_path = ::dbus::strings::Path::new(dbus_path).map_err(|_| ())?;

    let proxy = Proxy::new(
        "org.freedesktop.UDisks2",
        dbus_path,
        Duration::new(25, 0),
        &connection,
    );

    let options = UDisksOptions::new();
    let res: Result<(), _> =
        proxy.method_call("org.freedesktop.UDisks2.Drive", "Eject", (options,));

    if res.is_err() {
        return Err(());
    }

    Ok(())
}

fn udisks_unmount(dbus_path: &str) -> Result<(), ()> {
    let connection = Connection::new_system().map_err(|_| ())?;

    let dbus_path = ::dbus::strings::Path::new(dbus_path).map_err(|_| ())?;

    let proxy = Proxy::new(
        "org.freedesktop.UDisks2",
        dbus_path,
        Duration::new(25, 0),
        &connection,
    );

    let mut options = UDisksOptions::new();
    options.insert("force", Variant(Box::new(true)));
    let res: Result<(), _> =
        proxy.method_call("org.freedesktop.UDisks2.Filesystem", "Unmount", (options,));

    if let Err(err) = res {
        if err.name() != Some("org.freedesktop.UDisks2.Error.NotMounted") {
            return Err(());
        }
    }

    Ok(())
}

fn udisks_open(dbus_path: &str) -> Result<File, ()> {
    let connection = Connection::new_system().map_err(|_| ())?;

    let dbus_path = ::dbus::strings::Path::new(dbus_path).map_err(|_| ())?;

    let proxy = Proxy::new(
        "org.freedesktop.UDisks2",
        &dbus_path,
        Duration::new(25, 0),
        &connection,
    );

    let mut options = UDisksOptions::new();
    options.insert("flags", Variant(Box::new(libc::O_SYNC)));
    let res: (OwnedFd,) = proxy
        .method_call(
            "org.freedesktop.UDisks2.Block",
            "OpenDevice",
            ("rw", options),
        )
        .map_err(|_| ())?;

    Ok(unsafe { File::from_raw_fd(res.0.into_fd()) })
}
